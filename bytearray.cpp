#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "bytearray.h"

int bytearray_init(ByteArray *self, const char *string)
{
	self->count = self->size = strlen(string);
	self->bytes = (char *) calloc(1, self->size + 1);
	memcpy(self->bytes, string, self->size);
	return 0;
}

int bytearray_dtor(ByteArray *self)
{
	free(self->bytes), self->bytes = NULL;
	self->count = 0;
	self->size = 0;
	return 0;
}

int bytearray_join(ByteArray *self, ByteArray vector[], int n)
{

	// Allocate buffer on the stack. Since join string
	// is likely short, stack overflow is unlikelyt
	char *join_buffer = (char *) alloca(self->size + 1);
	size_t join_size = self->size;

	memcpy(join_buffer, self->bytes, self->size);
	
	// Don't include the last join string
	size_t new_size = (n - 1) * self->size;
	for (int i = 0; i < n; i++) {
		new_size += vector[i].size;
	}
	
	// Copied necessary info out into stack, so destroy it
	// And reinitialize it to contain the result of the joined string
	// This mimics the default python behavior
	bytearray_dtor(self);
	
	self->bytes = (char *) malloc(new_size + 1);
	
	int index = 0; // running counter
	for (int i = 0; i < (n - 1); i++) {
		memcpy(self->bytes + index, vector[i].bytes, vector[i].size);
		index += vector[i].size;
		memcpy(self->bytes + index, join_buffer, join_size);
		index += join_size;
	}
	// Copy the last string
	memcpy(self->bytes + index, vector[n-1].bytes, vector[n-1].size);
	self->size = new_size;		
	return 0;
}





int bytearray_init_from_vector(ByteArray bytes[], const char **vector, int size)
{
	for (int i = 0; i < size; i++) {
	        bytearray_init(bytes + i, vector[i]);	
	}
        return 0;
}


int bytearray_dtor_from_vector(ByteArray bytes[], int size)
{
    for (int i = 0; i < size; i++) {
        bytearray_dtor(bytes + i);
    }
}


int bytearray_compare_with_bytes(ByteArray *self, const char *string)
{
        return strcmp(self->bytes, string) == 0;
}

int bytearray_compare_with_vector(ByteArray bytes[], const char **vector, int size)
{
    for (int i = 0; i < size; i++)
        if (! bytearray_compare_with_bytes(bytes + i, vector[i]))
            return 0;
    return 1;

}
