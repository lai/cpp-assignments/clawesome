#include <unistd.h>
#include <string.h>
#include "bytearray.h"
#include "clawesome.h"
#include <stdio.h>


const POS C_START = {0, 3};


//  The order in which items are placed
//  in the claw machine at the start of the game
const char * const ITEMS_START[] = {
	ACORN,
	HEADPHONES,
	LOLLIPOP,
	HEART,
	MAGIC_WAND,
	BALL,
};


const char * C_CLOSED[] = {
	"  ||  ",
	" /  \\ ",
	" \\  / ",
};

const char * C_OPEN[] = {
	"  ||  ",
	" /  \\ ",
	"|    |",
};


const char * BOARD[] = {
        "================================|",
        "                                |",
        "                                |",
        "                                |",
        "                                |",
        "|    |                          |",
        "|    |==========================|",
};



void clear_screen(void)
{
        puts("\033[2J");
}

void frame_sleep(void)
{
        usleep(33000);
}

int CLAW_template_init(CLAW claw, const char **tmplate, int size)
{
	for (int i = 0; i  < size; i++) {
		bytearray_init(claw + i, tmplate[i]);
	}	
} 

int CLAW_dtor(CLAW claw)
{
	for (int i = 0; i < sizeof(CLAW)/sizeof(char *); i++) {
		if (claw[i].bytes) {
			bytearray_dtor(claw + i);
		}
	}
}

/*
 * @brief Copy item into claw
 * */
int claw_with_item(
	CLAW claw_vector,
	int vec_size,
	const char *item)
{
	ByteArray lastrow = claw_vector[vec_size-1];
	memcpy(lastrow.bytes + 2, item, strlen(item));	
}


int extend_claw(CLAW claw_vector, int vec_size, POS claw_pos)
{
	memmove(claw_vector + claw_pos[0], claw_vector, sizeof(ByteArray) * vec_size);
	for (int i = 0; i < claw_pos[0]; i++)
                bytearray_init(claw_vector + i, "  ||  ");
	vec_size = 3 + claw_pos[0];
	return vec_size;
}


int copy_objects(const MUTABLE_ITEMS items, MUTABLE_BOARD board)
{
	ByteArray item_row = board[BOARD_SIZE - 2];
	ByteArray temp[MAX_ITEMS];
	
	ByteArray joined;

	bytearray_init(&joined, "  ");

	for (int i = 0; i < MAX_ITEMS; i++)
		bytearray_init(temp + i, items[i]);

	bytearray_join(&joined, temp, MAX_ITEMS);
	memcpy(item_row.bytes + 8, joined.bytes, joined.size);
	bytearray_dtor(&joined);
	for (int i = 0; i < MAX_ITEMS; i++)
		bytearray_dtor(temp + i);
	return 0;

}


int copy_claw(CLAW claw_vector, int vec_size, POS claw_pos, MUTABLE_BOARD board)
{
    int new_size = extend_claw(claw_vector, vec_size, claw_pos);
    
    for (int i = 0; i < BOARD_SIZE ; i++) {
        if (i < new_size) {
            memcpy(board[i + 1].bytes + claw_pos[1], claw_vector[i].bytes, claw_vector[i].size);
        } 
    } 
}

#define ITEM_ZERO_OFFSET 6
#define NEXT_ITEM_OFFSET 4


const char *item_at_claw_pos(POS pos, MUTABLE_ITEMS items)
{
    int col = pos[1];
    if (col < ITEM_ZERO_OFFSET)
        return NULL;
    if ((col - ITEM_ZERO_OFFSET) % NEXT_ITEM_OFFSET == 0) {
        return items[(col - ITEM_ZERO_OFFSET) / NEXT_ITEM_OFFSET];
        
    }
    return NULL;
   
}


int render_frame(MUTABLE_BOARD board, POS claw_pos, int claw_closed, const char **items, const char *grabbed)
{
    ByteArray claw[BOARD_SIZE];
    
    bytearray_init_from_vector(claw, claw_closed ? C_CLOSED : C_OPEN, 3);
    claw_with_item(claw, 3, grabbed);
    copy_objects(items, board);
    copy_claw(claw, 3,  claw_pos, board);
    
    
    return 0;
}

static int draw_frame(POS claw_pos, int claw_closed, const char **items, const char *grabbed)
{
    clear_screen();
    MUTABLE_BOARD board;
    BOARD_INIT(board);
    render_frame(board, claw_pos, claw_closed, items, grabbed);
    for (int i = 0; i < BOARD_SIZE; i++) {
        puts(board[i].bytes);
    }
    BOARD_DTOR(board);
    frame_sleep();
    return 0;
}


static int item_index(const char *item, MUTABLE_ITEMS items)
{
    for (int i = 0; i < MAX_ITEMS; i++)
        if (strcmp(item, items[i]) == 0)
            return i;
    return -1;
}


int do_grab(MUTABLE_BOARD board, POS claw_pos, MUTABLE_ITEMS items)
{
    
    claw_pos[0] ++;
    draw_frame(claw_pos, 0, items, "  ");
    
    const char *target_item = item_at_claw_pos(claw_pos, items);
    if (target_item) {
        claw_pos[0] ++;
        draw_frame(claw_pos, 0, items, target_item);
        draw_frame(claw_pos, 1, items, target_item);
        int idx = item_index(target_item, items);
        items[idx] = "  ";
        claw_pos[0] --;
        draw_frame(claw_pos, 0, items, target_item);
          
        
    }
    else {
        target_item = "  ";
    }

    claw_pos[0] --;
    draw_frame(claw_pos, 1, items, target_item);
    
    int col = claw_pos[1];
    for (int i = 0; i < col; i++) {
        claw_pos[1] --;
        draw_frame(claw_pos, 1, items, target_item);
    }
    draw_frame(claw_pos, 0, items, "  ");
    
    return 0;
    

}


