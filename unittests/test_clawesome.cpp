#include <string.h>
#include "catch.hpp"
#include "mutation.h"
#include "../clawesome.h" 


TEST_CASE("Test claw with item")
{
	const char *expected0[] = {
		"  ||  ",
		" /  \\ ",
		"| db |",
	};

	const char *expected1[] = {
		"  ||  ",
		" /  \\ ",
		" \\db/ ",
	};

	CLAW  open_claw, closed_claw;
	CLAW_template_init(open_claw, C_OPEN, 3);
	CLAW_template_init(closed_claw, C_CLOSED, 3);
	MUTATION_START(claw_with_item, "clawesome.so")
	{
		typedef int (*TestFunc)(CLAW, int, const char *);
		TestFunc func = (TestFunc) test_function();
		func(open_claw, 3, "db");
		func(closed_claw, 3, "db");
		for (int i = 0; i < 3; i++) {
			REQUIRE(strcmp(expected0[i], open_claw[i].bytes) == 0);
			REQUIRE(strcmp(expected1[i], closed_claw[i].bytes) == 0);
		}
	}
	MUTATION_END

}
	


TEST_CASE("Test copy_objects")
{
	const char * expected_0[] = {
		"================================|",
		"                                |",
		"                                |",
		"                                |",
		"                                |",
		"|    |                          |",
		"|    |==========================|",
	};
	
// Empty items
	const char * items_0[] = {
		"  ", "  ", "  ", "  ", "  ", "  "
	};



	const char * expected_1[] = {
		"================================|",
		"                                |",
		"                                |",
		"                                |",
		"                                |",
		"|    |  rm  ls  ar  ld      ~O  |",
		"|    |==========================|",
	};
	
	const char * items_1[] = {
		"rm", "ls", "ar", "ld", "  ", "~O"
	};


	MUTATION_START(copy_objects, "clawesome.so")
	{
		typedef int (*CopyFunc)(MUTABLE_ITEMS, MUTABLE_BOARD);
		CopyFunc testfunc = (CopyFunc) test_function();
		
		MUTABLE_BOARD board;
		for (int i = 0; i < BOARD_SIZE; i++)
			bytearray_init(board + i, expected_0[i]);
		testfunc(items_0, board);		
		for (int i = 0; i < BOARD_SIZE; i++) {
			REQUIRE(strcmp(board[i].bytes, expected_0[i]) == 0);
			bytearray_dtor(board +i);
					
		}
		
		for(int i = 0; i < BOARD_SIZE; i++)
			bytearray_init(board + i, expected_0[i]);
		
		testfunc(items_1, board);
		
		for (int i = 0; i < BOARD_SIZE; i++) {
			REQUIRE(strcmp(board[i].bytes, expected_1[i]) == 0);
			bytearray_dtor(board+i);
		}
	}
	MUTATION_END

}


TEST_CASE("test extend claw")
{
    const char *expected_0[] = {
              "  ||  ",
              " /  \\ ",
              " \\  / ",

    };

    const char *expected_1[] = {
             "  ||  ",
             "  ||  ",
             " /  \\ ",
             " \\  / ",
                                     
    };

    const char * expected_2[] = {
                "  ||  ",
                "  ||  ",
                "  ||  ",
                " /  \\ ",
                "|    |",
    };

    ByteArray claw1[BOARD_SIZE];
    bytearray_init_from_vector(claw1, C_CLOSED, 3);
    ByteArray claw2[BOARD_SIZE];
    bytearray_init_from_vector(claw2, C_OPEN, 3);
    
    MUTATION_START(extend_claw, "clawesome.so")
    {

        
        ByteArray tmp[BOARD_SIZE];
        bytearray_init_from_vector(tmp, C_CLOSED, 3);
       
        POS pos = {0, 0};
        extend_claw(tmp, BOARD_SIZE, pos);        
        REQUIRE(bytearray_compare_with_vector(tmp, expected_0, 3));    
        bytearray_dtor_from_vector(tmp, 3);


        POS pos2 = {1, 0};
        extend_claw(claw1, BOARD_SIZE, pos2);      
        REQUIRE(bytearray_compare_with_vector(claw1, expected_1, 4));
/*
        POS pos3 = {2, 0};
        extend_claw(claw2, BOARD_SIZE, pos3);
        REQUIRE(bytearray_compare_with_vector(claw2, expected_2, 5));
*/

  }
    MUTATION_END  
    
}

TEST_CASE("copy claw")
{
    

    

    const char *expected_0[] = {
        "================================|",
        "  ||                            |",
        " /  \\                           |",
        "|    |                          |",
        "                                |",
        "|    |                          |",
        "|    |==========================|",
                                                     
    };

    const char *expected_1[] = {
        "================================|",
        "     ||                         |",
        "    /  \\                        |",
        "   |    |                       |",
        "                                |",
        "|    |                          |",
        "|    |==========================|",
    };
 
    const char *expected_2[] = {
        "================================|",
        "        ||                      |",
        "        ||                      |",
        "       /  \\                     |",
        "      |    |                    |",
        "|    |                          |",
        "|    |==========================|", 
    };

    
    MUTABLE_BOARD result0, result1, result2;
    ByteArray claw0[BOARD_SIZE];
    ByteArray claw1[BOARD_SIZE];
    ByteArray claw2[BOARD_SIZE];
    POS pos0 = {0, 0};
    POS pos1 = {0, 3};
    POS pos2 = {1, 6};
       

    BOARD_INIT(result0);
    BOARD_INIT(result1);
    BOARD_INIT(result2);
 
    bytearray_init_from_vector(claw0, C_OPEN, 3);
    bytearray_init_from_vector(claw1, C_OPEN, 3);
    bytearray_init_from_vector(claw2, C_OPEN, 3);

  
    MUTATION_START(copy_claw, "clawesome.so")
    {
        typedef int (*TestFunc)(CLAW, int, POS, MUTABLE_BOARD);          
        TestFunc func = (TestFunc) test_function();
        func(claw0, 3, pos0, result0);
        REQUIRE(bytearray_compare_with_vector(result0, expected_0, BOARD_SIZE));
        func(claw1, 3, pos1, result1);
        REQUIRE(bytearray_compare_with_vector(result1, expected_1, BOARD_SIZE));
        func(claw2, 3, pos2, result2);
        
        for (int i = 0; i < BOARD_SIZE; i++) puts(result2[i].bytes);
        for (int i = 0; i < BOARD_SIZE; i++) puts(expected_2[i]); 
        REQUIRE(bytearray_compare_with_vector(result2, expected_2, BOARD_SIZE));
    }
    MUTATION_END;
    
    
    BOARD_DTOR(result0);
    BOARD_DTOR(result1);
    BOARD_DTOR(result2);
}


TEST_CASE("item at claw pos")
{
    MUTABLE_ITEMS items = ITEMS_STATIC_INIT;
    POS pos0 = {0, 6};
    POS pos1 = {0, 10};
    POS pos2 = {0, 0};
    POS pos3 = {0, 11};

    MUTATION_START(item_at_claw_pos, "clawesome.so")
    {
        typedef const char * (*TestFunc)(POS, MUTABLE_ITEMS);
        TestFunc func = (TestFunc) test_function();
        REQUIRE(func);
        
        const char * result;
        result = func(pos0, items); 
        
        
        REQUIRE(strcmp(result, "{)") == 0);
        
        result = func(pos1, items);
        REQUIRE(strcmp(result, "db") == 0);
        result = func(pos2, items);
        REQUIRE(result == NULL);
        result = func(pos3, items);
        REQUIRE(result == NULL);
    } 
    MUTATION_END;
}
